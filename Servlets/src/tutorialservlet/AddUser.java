package tutorialservlet;

import org.hibernate.Session;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

@WebServlet(name = "AddUser", description = "Create new user Servlet", urlPatterns = { "/CreateUser.do"} )

public class AddUser extends HttpServlet {
    private static final long serialVersionUID=1L;

    public AddUser() {
        super();
    }

    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

        Session session = ConnectionManager.apriSessione();

        User u = new User(request.getParameter("firstname"), request.getParameter("lastname"), request.getParameter("country"), request.getParameter("birthday"));

        session.save(u);
        ConnectionManager.chiudiSessione(session);

        request.setAttribute("user", u);	// inseriamo l'oggetto User nella richiesta..
        RequestDispatcher view = request.getRequestDispatcher("useradd.jsp");  // ..e lo inviamo alla JSP
        view.forward(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

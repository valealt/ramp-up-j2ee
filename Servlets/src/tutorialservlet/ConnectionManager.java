package tutorialservlet;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class ConnectionManager {

    private static SessionFactory factory;

    static{

        Configuration config = new Configuration().configure();
        config.addAnnotatedClass(User.class);
        ServiceRegistry servReg = new StandardServiceRegistryBuilder().applySettings(config.getProperties()).build();
        factory = config.buildSessionFactory(servReg);
    }

    public static Session apriSessione(){
        Session s=factory.openSession();
        s.beginTransaction();

        return s;
    }

    public static void chiudiSessione(Session session){
        session.getTransaction().commit();
        session.close();
    }

}

package tutorialservlet;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.query.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@WebServlet(name = "DeleteUser", description = "Cancella utente", urlPatterns = {"/DeleteUser.do"})

public class DeleteUser extends HttpServlet {
    private static final long serialVersionUID=1L;

    public DeleteUser(){
        super();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Session session=ConnectionManager.apriSessione();

        int id=Integer.parseInt(request.getParameter("id"));
        User user = session.get(User.class,id);
        if( user != null ) {
            session.delete(user);
        }

        ConnectionManager.chiudiSessione(session);

        RequestDispatcher view = request.getRequestDispatcher("userdelete.jsp");
        view.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

}
/* modo alernativo agendo sulle query direttamente
        String sql = "DELETE FROM User WHERE id =:id ";
        Query query =(Query)session.createQuery(sql);
        query.setParameter("id",id);
        query.executeUpdate();*/

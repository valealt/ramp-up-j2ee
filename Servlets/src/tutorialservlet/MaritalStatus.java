package tutorialservlet;

import javax.persistence.*;


@Entity
@Table (name = "MARITALSTATUS")
public class MaritalStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "maritalstatus_id", nullable = false, unique = true)
    private Integer maritalId;

    @Column(name = "Status", length = 40, nullable = false)
    private String status;

    public MaritalStatus(String status){
        this.status = status;
    }

    public MaritalStatus(){}

    public Integer getMaritalId() {
        return maritalId;
    }

    public void setMaritalId(Integer maritalId) {
        this.maritalId = maritalId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

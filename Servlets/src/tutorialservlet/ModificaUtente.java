package tutorialservlet;

import org.hibernate.Session;

import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@WebServlet(name = "ModificaUtente",description = "Edit user Servlet", urlPatterns = { "/ModificaUser.do"})
public class ModificaUtente extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Session session = ConnectionManager.openSession();
        User user = new User();

        if ( request.getParameter("id") != null && !request.getParameter("id").equals("") ) {

            Integer userId = Integer.parseInt(request.getParameter("id"));
            user = session.get(User.class, userId);

        }

        String sql = "FROM MaritalStatus";
        Query queryM = session.createQuery(sql);
        List<MaritalStatus> maritalStatusList = queryM.getResultList();

        ConnectionManager.closeSession(session);

        request.setAttribute("user",user);
        request.setAttribute("maritalStatusList", maritalStatusList);

        RequestDispatcher view = request.getRequestDispatcher("modificautente.jsp");
        view.forward(request, response);

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Session session = ConnectionManager.openSession();

        String nome = request.getParameter("firstname");
        String cognome = request.getParameter("lastname");
        String paese = request.getParameter("country");
        String datanascita = request.getParameter("birthday");

        int maritalId = Integer.parseInt(request.getParameter("maritalstatus_id"));

        User user = new User();

        if ( request.getParameter("id") != null && !request.getParameter("id").equals("") ){

            Integer userId = Integer.parseInt(request.getParameter("id"));
            user.setId(userId);

        }
            user.setFirstname(nome);
            user.setLastname(cognome);
            user.setCountry(paese);
            user.setDateofbirth(datanascita);
            user.setMaritalStatus(session.get(MaritalStatus.class, maritalId));
            session.saveOrUpdate(user);

        ConnectionManager.closeSession(session);

        response.sendRedirect("SearchString.do");

    }
}

 /*Metodo alternativo di aggiornare i campi per la modifica invece di agire sulle session.update() nel doPost
        String sql= "UPDATE User SET firstname='"+nome+"', lastname='"+cognome+"' , country='"+paese+"', dateofbirth='"+datanascita+"' WHERE id=:id";
        Query query =session.createQuery(sql);
        query.setParameter("id",id);
        query.executeUpdate();*/

    /*Metodo alternativo per selezionare tutti gi utenti modificati e tornarli nella jsp collegata all'index nel doGet
        Query query = session.createQuery("SELECT u FROM User u WHERE id=:id");
        query.setParameter("id", userId);
        User utenteMod = (User) query.getSingleResult(); */
package tutorialservlet;

import org.hibernate.Session;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@WebServlet(name = "Search", description = "Search a string", urlPatterns = { "/SearchString.do"})
public class Search extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Session session = ConnectionManager.openSession();

        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(User.class);
        Root<User> rootUser = criteriaQuery.from(User.class);

        String charSequence = request.getParameter("stringaRicercata");

        criteriaQuery.select(rootUser);

        List <User> users = new ArrayList<>();

        if(charSequence != null && !charSequence.equals("")){
            String param = "%" + charSequence + "%";
            criteriaQuery.where(criteriaBuilder.or(criteriaBuilder.like(rootUser.get("firstname"),param),
                                criteriaBuilder.like(rootUser.get("lastname"),param),
                                criteriaBuilder.like(rootUser.get("country"),param),
                                criteriaBuilder.like(rootUser.get("dateofbirth"),param)));

            users = session.createQuery(criteriaQuery).getResultList();
        }

        request.setAttribute("users",users);

        ConnectionManager.closeSession(session);
        RequestDispatcher view = request.getRequestDispatcher("stampalista.jsp");
        view.forward(request, response);

    }

     protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

         Session session = ConnectionManager.openSession();

         CriteriaBuilder criteria = session.getCriteriaBuilder();
         CriteriaQuery<User> criteriaQuery = criteria.createQuery(User.class);
         Root<User> rootUser = criteriaQuery.from(User.class);
         criteriaQuery.select(rootUser);
         List<User> users = session.createQuery(criteriaQuery).getResultList();

         ConnectionManager.closeSession(session);

         request.setAttribute("users",users);
         RequestDispatcher view = request.getRequestDispatcher("stampalista.jsp");
         view.forward(request,response);

     }
}

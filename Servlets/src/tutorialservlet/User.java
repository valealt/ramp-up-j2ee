package tutorialservlet;

import javax.persistence.*;

@Entity
@Table( name = "USERS", uniqueConstraints = {@UniqueConstraint(columnNames = {"firstname","lastname"})})
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id", nullable = false, unique = true)
    private int id;

    @Column(name = "firstname", length = 40, nullable = false)
    private String firstname;

    @Column(name = "lastname", length = 40, nullable = false)
    private String lastname;

    @Column(name = "country", length = 40, nullable = true)
    private String country;

    @Column (name = "birthday", length = 40, nullable = true)
    private String dateofbirth;

    @ManyToOne (fetch = FetchType.EAGER)
    @JoinColumn(name = "maritalstatus_id")
    private MaritalStatus maritalStatus;

    public User(String fn, String ln, String country, String dateofbirth) {
        firstname = fn;
        lastname = ln;
        this.country = country;
        this.dateofbirth = dateofbirth;
    }

    public User() {
    }

    public int getId(){ return id;}

    public void setId(int id){
        this.id = id;
    }

    public String getFirstname(){
        return firstname;
    }

    public void setFirstname(String firstname){
        this.firstname = firstname;
    }

    public String getLastname(){
        return lastname;
    }

    public void setLastname(String lastname){
        this.lastname = lastname;
    }

    public String getCountry(){
        return country;
    }

    public void setCountry(String country){
        this.country = country;
    }

    public String getDateofbirth(){
        return this.dateofbirth;
    }

    public void setDateofbirth(String data){
        this.dateofbirth = data;
    }

    public MaritalStatus getMaritalStatus(){
        return this.maritalStatus;
    }

    public void setMaritalStatus(MaritalStatus maritalStatus){
        this.maritalStatus = maritalStatus;
    }
}
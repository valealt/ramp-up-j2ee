<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"  import="tutorialservlet.User" %>

<html>
<head>
    <title> Modifica utente</title>
</head>
<body bgcolor = "#ffb6c1">
<h1><font color = "#8a2be2"> Inserisci i tuoi dati </font></h1>
<form method = "POST" action = "ModificaUser.do">
    <p>
        First name: <input type = "text" size = "40" maxlength = "40" name = "firstname"  value = "${user.firstname}" > <br>
        <br>
        Last name:  <input type = "text" size = "40" maxlength = "40" name = "lastname"  value = "${user.lastname}" > <br>
        <br>
        Country:    <input type = "text" size = "40" maxlength = "40" name = "country"   value = "${user.country}" > <br>
        <br>
        Date of birth: <input type = "date" size = "40" maxlength = "40" name = "birthday" value = "${user.dateofbirth}" > <br>
        <br>
        Stato civile:
        <select name="maritalstatus_id" >
            <c:forEach items = "${maritalStatusList}" var = "maritalStatus">
                <option value = "${maritalStatus.maritalId}" ${maritalStatus.maritalId eq user.maritalStatus.maritalId ? 'selected' : ''}>
                        ${maritalStatus.status}
                </option>
            </c:forEach>
        </select>
        <input type = "hidden" name = "id" value = "${user.id}" >
    </p>
<br>
<br>
    <input type = "SUBMIT" name = "submit" style = "background-color: dodgerblue"  value ="Salva" >
<br/>
<br/>
</form>
<br/>
<br/>
<a href = "/"> torna all'homepage</a>
</body>
</html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<head>
    <meta http-equiv = "Content type" content = "text/html"; charset = UTF-8">
    <title> Benvenuti in questa nuova app! </title>
</head>
<body bgcolor = "#ffb6c1" >

<h1> <p align = "center" > <font color = "blue" face = "bold" > Benvenuti nel sito! </font> </p> </h1>
<br>
<h2> <p align = "center" > <a href = "/ModificaUser.do" > Crea nuovo utente </a> </p> </h2>

<h4> <font color = "#8a2be2" > Utenti registrati: </font> </h4>
<br>
<form method = "POST" action = "/SearchString.do">
<table id = "MiaTable" border = "2" bgcolor = "#ffdead" >

    <th> <font color = "#dc143c" > Nome </font> </th>
    <th> <font color = "#dc143c" > Cognome </font> </th>
    <th> <font color = "#dc143c" > Paese </font> </th>
    <th> <font color = "#dc143c" > Data di Nascita </font> </th>
    <th> <font color = "#dc143c" > Stato civile </font> </th>
    <th> <font color = "#dc143c" > Action </font> </th>

  <tbody>
    <c:forEach items = "${users}" var = "user">
      <tr class = "header" >
        <td> ${user.firstname} </td>
        <td> ${user.lastname} </td>
        <td> ${user.country} </td>
        <td> ${user.dateofbirth} </td>
        <td> ${user.maritalStatus.status} </td>

        <td>
          <form method = "POST" action = "DeleteUser.do" >
                <input type = "hidden"  name = "id" value = ${user.id} >
                <input type = "SUBMIT" style = "background-color: tomato"  value = "Delete" >
            </form>
        </td>

        <td>
            <form method = "GET" action = "ModificaUser.do" >
                <input type = "hidden"  name = "id" value = ${user.id} >
                <input type = "SUBMIT" style = "background-color: dodgerblue"  value = "Edit" >
            </form>
        </td>
      </tr>
    </c:forEach>
</form>
  </tbody>
</table>
<br>
<br>
<br>
<br>
Ricerca la tua parola--><input type = "text" size = "40" maxlength = "40" style = "background-color:#ffdead" name = "stringaRicercata" />
<br>
<br>
    <c:if test="${not empty param.stringaRicercata}">
        <c:url var="homeurl" value="/" />
        <span> Hai cercato ${param.stringaRicercata} </span>
        <p align="center"> <a href = "${homeurl}"> torna all'homepage </a> </p>
    </c:if>
<br>
</body>
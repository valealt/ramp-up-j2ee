<%@ page contentType="text/html;charset=ISO-8859-1" language="java" pageEncoding="ISO-8859-1" import="tutorialservlet.User" %>
<html>
<head>
 <meta http-equiv="Content-Type" content="text/html" charset="ISO-8859-1">
    <title>nuovo utente creato</title>
</head>
<body>
    <h1> Nuovo utente creato </h1>
    <p>
      <% User u = (User)request.getAttribute("user"); %>
      First Name: <% String fname = request.getParameter("firstname"); out.print(fname); %> <br/>
      Last Name: <% String lname = request.getParameter("lastname"); out.print(lname); %> <br/>
      Country: <% String country = request.getParameter("country"); out.print(country); %> <br/>
      Date of birth: <% String bday =request.getParameter("birthday"); out.print(bday); %>
    </p>
</body>
</html>
